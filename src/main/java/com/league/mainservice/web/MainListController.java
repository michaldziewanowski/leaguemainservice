package com.league.mainservice.web;

import com.league.mainservice.Setup;
import com.league.mainservice.model.ChampionEntity;
import com.league.mainservice.model.NewsEntity;
import com.league.mainservice.model.SummonerMainEntity;
import com.league.mainservice.repository.ChampionRepository;
import com.league.mainservice.repository.NewsRepository;
import com.league.mainservice.repository.SummonerMainRepository;
import com.league.mainservice.service.BestPlayersService;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.static_data.dto.Champion;
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class MainListController {

	@Autowired
	private ChampionRepository championRepository;

	@Autowired
	private SummonerMainRepository summonerMainRepository;

	@Autowired
    private NewsRepository newsRepository;

	@Autowired
	private BestPlayersService bestPlayersService;

	@RequestMapping(value = "/getTopTen/{name}", method = RequestMethod.GET)
	public ResponseEntity<List<SummonerMainEntity>> getTop10(@PathVariable("name") String name)
            throws RiotApiException {

        ChampionEntity champion = championRepository.findByChampionName(name);
        List<SummonerMainEntity> response = bestPlayersService.getBest10PlayersWithChampion(champion);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/get/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<SummonerMainEntity>> getMain(@PathVariable("name") String name) {
        List<SummonerMainEntity> all = (List<SummonerMainEntity>) summonerMainRepository.findAll();
        List<SummonerMainEntity> result = all
                .stream()
                .filter(val -> val.getChampionName().equals(name))
                .collect(Collectors.toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome() throws RiotApiException {
		return "Service works fine";
	}

	@GetMapping(value = "/image/{name}", produces = MediaType.IMAGE_PNG_VALUE)
	public BufferedImage image(@PathVariable String name) throws IOException {
		char[] nameChar = name.toCharArray();
		for (int i = 0; i < name.length(); ++i) {
			if ((nameChar[i] >= 'a' && nameChar[i] <= 'z')
					|| (nameChar[i] >= 'A' && nameChar[i] <= 'Z')) continue;
			nameChar[i] = '.';
		}
		File file = new File(Setup.CHAMPION_SQUARE_DIR
				+ new String(nameChar).replace(	".", "") + ".png");

		return ImageIO.read(file);
	}

	@RequestMapping(value = "/champions", method = RequestMethod.GET)
	public ResponseEntity<List<ChampionEntity>> champions() throws RiotApiException {
		List<ChampionEntity> champions = (List<ChampionEntity>) championRepository.findAll();
		return new ResponseEntity<>(champions, HttpStatus.OK);
	}

	@RequestMapping(value = "/static/champions")
	public void updateChampions() throws RiotApiException {
		ChampionList list = bestPlayersService.getChampions();
		Map<String, Champion> map = list.getData();

		for (Map.Entry<String, Champion> entry : map.entrySet()) {
			ChampionEntity entity = new ChampionEntity();
			entity.setChampionId(entry.getValue().getId());
			entity.setChampionName(entry.getValue().getName());
			championRepository.save(entity);
		}
	}

	@RequestMapping(value = "/news/upload", method = RequestMethod.PUT)
    public void uploadNews(@RequestBody List<NewsEntity> newsEntity) {
	    newsRepository.saveAll(newsEntity);
    }

    @RequestMapping(value = "/news/get", method = RequestMethod.GET)
    public ResponseEntity<List<NewsEntity>> getAllNews() {
	    List<NewsEntity> list = (List<NewsEntity>) newsRepository.findAll();
	    return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/news/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<NewsEntity> getNewsById(@PathVariable Long id) {
        Optional<NewsEntity> optional = newsRepository.findById(id);
        NewsEntity news = null;
        if (optional.isPresent()) {
            news = optional.get();
        }
        return news == null ?
                new ResponseEntity<>(news, HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(news, HttpStatus.OK);
    }

    @RequestMapping(value = "/news/delete", method = RequestMethod.DELETE)
    public void removeNews() {
	    newsRepository.deleteAll();
    }

    @RequestMapping(value = "/news/delete/{id}", method = RequestMethod.DELETE)
    public void removeNewsById(@PathVariable Long id) {
	    newsRepository.deleteById(id);
    }
}
