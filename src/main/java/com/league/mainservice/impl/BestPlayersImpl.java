package com.league.mainservice.impl;

import com.league.mainservice.Setup;
import com.league.mainservice.SummonerChampionPair;
import com.league.mainservice.model.ChampionEntity;
import com.league.mainservice.model.SummonerMainEntity;
import com.league.mainservice.repository.ChampionRepository;
import com.league.mainservice.service.BestPlayersService;
import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.champion_mastery.dto.ChampionMastery;
import net.rithms.riot.api.endpoints.league.constant.LeagueQueue;
import net.rithms.riot.api.endpoints.league.dto.LeagueItem;
import net.rithms.riot.api.endpoints.league.dto.LeagueList;
import net.rithms.riot.api.endpoints.league.dto.LeaguePosition;
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList;
import net.rithms.riot.api.endpoints.summoner.dto.Summoner;
import net.rithms.riot.constant.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BestPlayersImpl implements BestPlayersService {

	@Autowired
	ChampionRepository championRepository;

	private RiotApi api;

	public BestPlayersImpl() {
		api = new RiotApi(new ApiConfig().setKey(Setup.API_KEY));
	}

	@Override
	public List<SummonerMainEntity> getBest10PlayersWithChampion(ChampionEntity champion)
			throws RiotApiException {

		LeagueList challengerLeagueByQueue = api.getChallengerLeagueByQueue(Platform.EUNE, LeagueQueue.RANKED_SOLO_5x5);

		List<LeagueItem> entries = challengerLeagueByQueue.getEntries();
		List<SummonerChampionPair> summoners = new ArrayList<>();

		int k = 0;
		for (LeagueItem item : entries) {
			if (summoners.size() == 10 || k++ == 15) break;
			String name = item.getPlayerOrTeamName();
			Summoner summoner = api.getSummonerByName(Platform.EUNE, name);
			ChampionMastery champMastery;
			try {
				champMastery = api.getChampionMasteriesBySummonerByChampion(
						Platform.EUNE,
						summoner.getId(),
						champion.getChampionId());
			} catch (RiotApiException e) {
				System.out.println("GOT ERROR : "+e.getMessage());
				continue;
			}
			summoners.add(new SummonerChampionPair(summoner, champMastery));
		}
		summoners.sort((o1, o2) -> Long.compare(
				o1.getChampionMastery().getChampionPoints(),
				o2.getChampionMastery().getChampionPoints()) * -1);

		List<SummonerChampionPair> result = new ArrayList<>();
		for (int i = 0; i < Math.min(summoners.size(), 10); ++i) {
			result.add(summoners.get(i));
		}

		List<SummonerMainEntity> list = new ArrayList<>();
		for (SummonerChampionPair pair : result) {
			Set<LeaguePosition> positions = api.getLeaguePositionsBySummonerId(Platform.EUNE, pair.getSummoner().getId());
			LeaguePosition actual = null;

			for (LeaguePosition current : positions) {
				if (current.getQueueType().contains("SOLO")) {
					actual = current;
				}
			}

			SummonerMainEntity summonerMainEntity = new SummonerMainEntity();
			summonerMainEntity.setSummonerName(pair.getSummoner().getName());
			summonerMainEntity.setSummonerLevel(pair.getSummoner().getSummonerLevel());
			summonerMainEntity.setSummonerRank(actual.getTier() + " " + actual.getRank());
			summonerMainEntity.setChampionName(champion.getChampionName());
			summonerMainEntity.setChampionPoints(pair.getChampionMastery().getChampionPoints());
			list.add(summonerMainEntity);
		}
		return list;
	}

	@Override
	public ChampionList getChampions() throws RiotApiException {
		return api.getDataChampionList(Platform.EUNE);
	}
}
