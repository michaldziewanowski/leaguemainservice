package com.league.mainservice.service;

import com.league.mainservice.model.ChampionEntity;
import com.league.mainservice.model.SummonerMainEntity;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList;

import java.util.List;

public interface BestPlayersService {

	List<SummonerMainEntity> getBest10PlayersWithChampion(ChampionEntity champion) throws RiotApiException;

	ChampionList getChampions() throws RiotApiException;

}