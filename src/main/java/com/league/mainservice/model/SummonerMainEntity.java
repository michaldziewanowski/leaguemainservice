package com.league.mainservice.model;

import javax.persistence.*;

@Entity
@Table(name = "summoner_main")
public class SummonerMainEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "summoner_name")
	private String summonerName;

    @Column(name = "summoner_rank")
	private String summonerRank;

    @Column(name = "champion_name")
	private String championName;

    @Column(name = "summoner_level")
	private int summonerLevel;

    @Column(name = "champion_points")
	private long championPoints;

	public String getSummonerName() {
		return summonerName;
	}

	public void setSummonerName(String summonerName) {
		this.summonerName = summonerName;
	}

	public String getSummonerRank() {
		return summonerRank;
	}

	public void setSummonerRank(String summonerRank) {
		this.summonerRank = summonerRank;
	}

	public String getChampionName() {
		return championName;
	}

	public void setChampionName(String championName) {
		this.championName = championName;
	}

	public Long getChampionPoints() {
		return championPoints;
	}

	public void setChampionPoints(Long championPoints) {
		this.championPoints = championPoints;
	}

	public int getSummonerLevel() {
		return summonerLevel;
	}

	public void setSummonerLevel(int summonerLevel) {
		this.summonerLevel = summonerLevel;
	}
}
