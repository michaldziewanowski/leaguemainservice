package com.league.mainservice.repository;

import com.league.mainservice.model.SummonerMainEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SummonerMainRepository extends CrudRepository<SummonerMainEntity, Long> {
}
