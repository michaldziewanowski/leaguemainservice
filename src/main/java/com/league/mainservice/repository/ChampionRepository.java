package com.league.mainservice.repository;

import com.league.mainservice.model.ChampionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChampionRepository extends CrudRepository<ChampionEntity, Long> {

	ChampionEntity findByChampionName(String championName);
}
