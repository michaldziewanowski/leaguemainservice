package com.league.mainservice;

import net.rithms.riot.api.endpoints.champion_mastery.dto.ChampionMastery;
import net.rithms.riot.api.endpoints.summoner.dto.Summoner;

public class SummonerChampionPair {
	Summoner summoner;
	ChampionMastery championMastery;

	public SummonerChampionPair(Summoner summoner, ChampionMastery championMastery) {
		this.summoner = summoner;
		this.championMastery = championMastery;
	}

	public Summoner getSummoner() {
		return summoner;
	}

	public void setSummoner(Summoner summoner) {
		this.summoner = summoner;
	}

	public ChampionMastery getChampionMastery() {
		return championMastery;
	}

	public void setChampionMastery(ChampionMastery championMastery) {
		this.championMastery = championMastery;
	}
}
